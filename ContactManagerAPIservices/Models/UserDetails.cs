﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactManagerAPIservices.Models
{
    public class UserDetails
    {
        public int? userId { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public int? zipCode { get; set; }

        public string city { get; set; }

        public string state { get; set; }


    }
}
