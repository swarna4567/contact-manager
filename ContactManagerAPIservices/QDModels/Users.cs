﻿using System;
using System.Collections.Generic;

namespace ContactManagerAPIservices.QDModels
{
    public partial class Users
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}
