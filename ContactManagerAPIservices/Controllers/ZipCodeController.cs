﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ContactManagerAPIservices.Controllers
{
    [Route("api/[Controller]")]
    public class ZipCodeController : Controller
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}
        [HttpGet]
        public async Task<List<int>> GetZipCodes()
        {
            var zipCodeInfo = new List<int>();
            using (var db = new QDModels.QDContext())
            {
                zipCodeInfo = await (from s in db.ZipCodes
                                     select s.ZipCode).ToListAsync();

            };

            return zipCodeInfo;
        }

    }
}