﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ContactManagerAPIservices.Models;
using Microsoft.EntityFrameworkCore;

namespace ContactManagerAPIservices.Controllers
{
    [Route("api/[Controller]")]
    public class UsersController : Controller
    {

        [HttpGet]
        public async Task<List<UserDetails>> GetUsers()
        {
            var userInfo = new List<UserDetails>();
            using (var db = new QDModels.QDContext())
            {
                userInfo = await (from s in db.Users
                                  select new UserDetails
                                  {
                                      userId = s.UserId,
                                      firstName = s.FirstName,
                                      lastName = s.LastName,
                                      zipCode = s.ZipCode,
                                      city = s.City,
                                      state = s.State
                                  }).ToListAsync();

            };

            return userInfo;
        }

        [HttpPost]
        public async Task<UserDetails> CreateUsers(UserDetails newuser)
        {
            using (var db = new QDModels.QDContext())
            {
                var User = new QDModels.Users()
                {
                    FirstName = newuser.firstName,
                    LastName = newuser.lastName,
                    ZipCode = newuser.zipCode,
                    City = newuser.city,
                    State = newuser.state
                };
                db.Users.Add(User);
                await db.SaveChangesAsync().ConfigureAwait(false);

                newuser.userId = User.UserId;

            };
            return newuser;
        }

        [HttpPut]
        public async  Task<bool> UpdateUsers(UserDetails users)
        {
            var result = false;

            using (var db = new QDModels.QDContext())
            {
                var updateuser = (from c in db.Users
                                  where c.UserId == users.userId
                                  select c).FirstOrDefault();
                updateuser.FirstName = users.firstName;
                updateuser.LastName = users.lastName;
                updateuser.ZipCode = users.zipCode;
                updateuser.City = users.city;
                updateuser.State = users.state;
                await db.SaveChangesAsync();
                result = true;

            };
            return result;

        }

        [HttpDelete]
        public async Task<bool> DeleteUsers(int Id)
        {
            var result = false;
            using (var db = new QDModels.QDContext())
            {
                    db.Users.Remove(db.Users.FirstOrDefault(e => e.UserId == Id));
                    await db.SaveChangesAsync();
                    result = true;
                
            };
            return result;
        }

        
    }
}