﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ContactMangerUI.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text;
using System.Globalization;
using System.IO;
using Kendo.Mvc;
using Flurl.Http;

namespace ContactMangerUI.Controllers
{

    public class HomeController : Controller
    {
        public static ContactManagerAPIservices.ContactManagerAPIservicesClient ApiClientConnect()
        {
            var contactmanagerAPIClient = new ContactManagerAPIservices.ContactManagerAPIservicesClient();
            contactmanagerAPIClient.BaseUri = new System.Uri("http://localhost:62992/");
            contactmanagerAPIClient.HttpClient.Timeout = TimeSpan.FromSeconds(60);
            return contactmanagerAPIClient;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<CityInfo> GetCity(int? Zipcode)
        {
            var cityInfo = await $"http://ziptasticapi.com/{Zipcode}".GetJsonAsync<CityInfo>();
            return cityInfo;
        }

        public async Task<IActionResult> Users_Read([DataSourceRequest] DataSourceRequest request)
        {
            var contactmanagerAPIClient = ApiClientConnect();
            var usersInfo = await contactmanagerAPIClient.GetUsersWithHttpMessagesAsync();
            var uresult = usersInfo.Body.Select(p => new { p.UserId, p.FirstName, p.LastName, p.ZipCode, p.City, p.State }).ToList();
            return Json(uresult.ToDataSourceResult(request));
        }

        public async Task<ActionResult> Users_Create([DataSourceRequest]DataSourceRequest request, UsersInfo model)
        {
            if (ModelState.IsValid)
            {
                var cityInfo = await GetCity(model.ZipCode);
                var contactmanagerAPIClient = ApiClientConnect();
                var usersContact = await contactmanagerAPIClient.CreateUsersWithHttpMessagesAsync(null, model.FirstName, model.LastName, model.ZipCode, cityInfo.City, cityInfo.State);
                var user = usersContact.Body;
                var result = new UsersInfo()
                {
                    UserId = user.UserId ?? 0,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    ZipCode = user.ZipCode,
                    City = cityInfo.City,
                    State = cityInfo.State
                };

                return Json(new[] { result }.ToDataSourceResult(request, ModelState));
            }
            else
            {
                return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            }
           
        }

        public async Task<ActionResult> Users_Update([DataSourceRequest]DataSourceRequest request, UsersInfo model)
        {
            if (ModelState.IsValid)
            {
                var cityInfo = await GetCity(model.ZipCode);
                var contactmanagerAPIClient = ApiClientConnect();
                var users = await contactmanagerAPIClient.UpdateUsersWithHttpMessagesAsync(model.UserId, model.FirstName, model.LastName, model.ZipCode, cityInfo.City, cityInfo.State);
                var UsersResult = users.Body;

                var result = new UsersInfo()
                {
                    UserId = model.UserId ?? 0,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    ZipCode = model.ZipCode,
                    City = cityInfo.City,
                    State = cityInfo.State
                };
                return Json(new[] { result }.ToDataSourceResult(request, ModelState));
            }
            else
            {
                return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            }
            
        }

        public async Task<ActionResult> Users_Delete([DataSourceRequest]DataSourceRequest request, UsersInfo model)
        {
            var contactmanagerAPIClient = ApiClientConnect();
            var UsersId = await contactmanagerAPIClient.DeleteUsersWithHttpMessagesAsync(model.UserId);
            var UsersResult = UsersId.Body;

            return Json(new[] { model }.ToDataSourceResult(request));
        }
    }
}
