﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactMangerUI.Models
{
    public class CityInfo
    {
        public String Country { get; set; }
        public String State { get; set; }
        public String City { get; set; }

    }
}
