using System;

namespace ContactMangerUI.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public int stem { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}