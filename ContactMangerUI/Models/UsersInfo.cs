﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ContactMangerUI.Models
{
    public class UsersInfo
    {
        [ScaffoldColumn(false)]
        public int? UserId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public int? ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
       
    }
}
